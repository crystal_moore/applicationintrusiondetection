<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <artifactId>applicationintrusiondetection</artifactId>
    <groupId>de.dominikschadow</groupId>
    <version>0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>Application Intrusion Detection</name>
    <url>https://github.com/dschadow/ApplicationIntrusionDetection</url>
    <description>This repository contains application intrusion detection sample projects based on OWASP AppSensor.
    </description>

    <organization>
        <name>Dominik Schadow</name>
        <url>http://www.dominikschadow.de</url>
    </organization>

    <properties>
        <docker.image.prefix>dschadow</docker.image.prefix>
        <appsensor.version>2.2.0</appsensor.version>
        <boxfuse.version>1.13.4.789</boxfuse.version>
    </properties>

    <prerequisites>
        <maven>3.0.0</maven>
    </prerequisites>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.3.0.RELEASE</version>
    </parent>

    <issueManagement>
        <system>GitHub</system>
        <url>https://github.com/dschadow/ApplicationIntrusionDetection/issues</url>
    </issueManagement>

    <scm>
        <connection>scm:git:git://github.com/dschadow/ApplicationIntrusionDetection</connection>
    </scm>

    <repositories>
        <repository>
            <id>gemfire-repository</id>
            <name>Gemfire Repository</name>
            <url>http://dist.gemstone.com/maven/release</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>boxfuse-repo</id>
            <url>https://files.boxfuse.com</url>
        </pluginRepository>
    </pluginRepositories>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-core</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-local</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-storage-in-memory</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-analysis-reference</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-reporting-simple-logging</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-access-control-reference</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-configuration-stax</artifactId>
                <version>${appsensor.version}</version>
            </dependency>
            <dependency>
                <groupId>org.owasp.appsensor</groupId>
                <artifactId>appsensor-integration-spring-security</artifactId>
                <version>${appsensor.version}</version>
                <exclusions>
                    <exclusion>
                        <artifactId>spring-security-core</artifactId>
                        <groupId>org.springframework.security</groupId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.owasp</groupId>
                <artifactId>security-logging-logback</artifactId>
                <version>1.1.0</version>
            </dependency>
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>1.4.190</version>
            </dependency>
            <dependency>
                <groupId>org.webjars</groupId>
                <artifactId>bootstrap</artifactId>
                <version>3.3.5</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <defaultGoal>clean package</defaultGoal>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>2.2</version>
                    <configuration>
                        <newVersion>${env.TRAVIS_BUILD_NUMBER}</newVersion>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <encoding>${project.build.sourceEncoding}</encoding>
                        <source>1.8</source>
                        <target>1.8</target>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>2.6</version>
                    <configuration>
                        <delimiters>
                            <delimiter>@</delimiter>
                        </delimiters>
                    </configuration>
                </plugin>
                <!-- Boxfuse requires credentials as described here: https://boxfuse.com/docs/maven/ -->
                <plugin>
                    <groupId>com.boxfuse.client</groupId>
                    <artifactId>boxfuse-maven-plugin</artifactId>
                    <version>${boxfuse.version}</version>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>0.3.5</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-scm-plugin</artifactId>
                    <version>1.9.4</version>
                </plugin>
                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.8.2</version>
                    <configuration>
                        <skip>true</skip>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>2.8.1</version>
            </plugin>
            <plugin>
                <groupId>org.owasp</groupId>
                <artifactId>dependency-check-maven</artifactId>
                <version>1.3.1</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>aggregate</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
        </plugins>
    </reporting>

    <modules>
        <module>duke-encounters</module>
    </modules>
</project>